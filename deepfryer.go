package deepfryer

import (
	"fmt"
	"github.com/anthonynsimon/bild/channel"
	"gopkg.in/gographics/imagick.v2/imagick"
	"image"
	"image/color"
	"math"
	"path"
	"runtime"
)

type DeepFryConfig struct {
	Levels      uint
	ResizeBlur  float64
	Brightness  float64
	Contrast    float64
	Compression uint
	Swirl       float64
}

type Flare struct {
	Position image.Point
	Size     uint
}

func createPixelWand(color color.RGBA) *imagick.PixelWand {
	pixelWand := imagick.NewPixelWand()
	pixelWand.SetColor(fmt.Sprintf("rgb(%d, %d, %d)", color.R, color.G, color.B))
	return pixelWand
}

func createFlare(config Flare, path string) *imagick.MagickWand {
	flare := imagick.NewMagickWand()
	flare.ReadImage(path + "/flares/flare.png")
	flare.AdaptiveResizeImage(config.Size, config.Size)
	return flare
}

func powerSize(size uint, ratio float64) uint {
	return uint(math.Pow(float64(size), ratio))
}

func Deepfry(config DeepFryConfig, flares []Flare, source string) *imagick.MagickWand {
	_, filename, _, ok := runtime.Caller(0)
	if !ok {
		panic("No caller information")
	}

	if !imagick.IsCoreInstantiated() {
		imagick.Initialize()
	}

	mw := imagick.NewMagickWand()

	err := mw.ReadImage(source)
	if err != nil {
		panic(err)
	}
	width := mw.GetImageWidth()
	height := mw.GetImageHeight()

	mw.ResizeImage(powerSize(width, 0.75), powerSize(height, 0.75), imagick.FILTER_LANCZOS, 1)
	mw.ResizeImage(powerSize(width, 0.88), powerSize(height, 0.88), imagick.FILTER_TRIANGLE, 1)
	mw.ResizeImage(powerSize(width, 0.9), powerSize(height, 0.9), imagick.FILTER_CATROM, 1)
	mw.ResizeImage(width, height, imagick.FILTER_CATROM, config.ResizeBlur)
	mw.PosterizeImage(config.Levels, false)

	red := mw.Clone()
	defer red.Destroy()
	red.SeparateImageChannel(channel.Red)
	mw.BrightnessContrastImage(config.Brightness, config.Contrast)

	// create colorise wands
	redPixelWand := createPixelWand(color.RGBA{254, 0, 2, 255})
	yellowPixelWand := createPixelWand(color.RGBA{255, 255, 15, 255})
	blackPixelWand := createPixelWand(color.RGBA{0, 0, 0, 255})
	whitePixelWand := createPixelWand(color.RGBA{255, 255, 255, 255})

	red.ColorizeImage(yellowPixelWand, blackPixelWand)
	red.ColorizeImage(redPixelWand, whitePixelWand)
	red.SetImageOpacity(0.75)

	mw.CompositeImage(red, imagick.COMPOSITE_OP_OVERLAY, 0, 0)

	mw.OpaquePaintImage(whitePixelWand, yellowPixelWand, 10, false)

	// swirl the image
	mw.SwirlImage(config.Swirl)

	mw.SharpenImage(20, 1)

	for _, flareConfig := range flares {
		flare := createFlare(flareConfig, path.Dir(filename))
		defer flare.Destroy()
		mw.CompositeImage(flare, imagick.COMPOSITE_OP_OVER, flareConfig.Position.X, flareConfig.Position.Y)
	}

	mw.SetImageCompressionQuality(config.Compression)
	runtime.KeepAlive(mw)
	return mw
}
