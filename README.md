![pipeline](https://gitlab.com/ruivieira/deepfryer/badges/master/build.svg)

# deepfryer

![](examples/open.jpg)             |  ![](examples/spongebob.jpg) | ![](examples/homer.jpg)     
:-------------------------:|:-------------------------:|:----------------------------
![](examples/open_deepfryed.jpg)  |  ![](examples/spongebob_deepfryed.jpg) | ![](examples/homer_deepfryed.jpg)

## setup

### macOS

Make sure you have ImageMagick (IM) 6.x installed:
```
brew install imagemagick@6
```
Since IM@6 is "keg-only", add the linking environment variables:

```
echo 'export PATH="/usr/local/opt/imagemagick@6/bin:$PATH"' >> ~/.zshrc
export LDFLAGS="-L/usr/local/opt/imagemagick@6/lib"
export CPPFLAGS="-I/usr/local/opt/imagemagick@6/include"
```
and the `pkg-config` path:
```
export PKG_CONFIG_PATH="/usr/local/opt/imagemagick@6/lib/pkgconfig"
```  

## usage

Import the `deepfryed` package:

```go
import (
	"deepfryer/deepfryer"
)
```

Create a `DeepFryedConfig` specifying
* `Levels`, final colour bits
* `ResizeBlur`, blur level for resize-mangling
* `Brightness` and `Contrast` for the deepfry dominant colour overlay
* `Swirl`, the radial distortion (in degrees, `0` or empty for no distortion)

```go
var config = deepfryer.DeepFryConfig{
		Levels:     7,
		ResizeBlur: 2.0,
		Brightness: 1,
		Contrast:   2,
	}
```
If you want flares, specify `Size` (absolute size, pixels), position and add them to an array.
```go
var flares = make([]deepfryer.Flare, 2)
	flares[0] = deepfryer.Flare{
		Position: image.Point{x1, x2},
		Size:     size1,
	}
	flares[1] = deepfryer.Flare{
		Position: image.Point{x3, x4},
		Size:     size2,
	}
```
Turn on the deepfryer.   
Specify absolute path for input image and then save output.
```go
output := deepfryer.Deepfry(
		config,
		flares,
		"./input.jpg")
output.WriteImage("./output.jpg")

```

Examples can be found in the `examples` folder.