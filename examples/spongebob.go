package main

import (
	"gitlab.com/ruivieira/deepfryer"
	"image"
)

func main() {
	var config = deepfryer.DeepFryConfig{
		Levels:      7,
		ResizeBlur:  2.0,
		Brightness:  1,
		Contrast:    2,
		Compression: 20,
		Swirl:       0,
	}
	flareSize := uint(75)
	flareX := 60
	flareY := 21
	var flares = make([]deepfryer.Flare, 2)
	flares[0] = deepfryer.Flare{
		Position: image.Point{flareX, flareY},
		Size:     flareSize,
	}
	flares[1] = deepfryer.Flare{
		Position: image.Point{flareX + 30, flareY + 5},
		Size:     flareSize,
	}
	output := deepfryer.Deepfry(
		config,
		flares,
		"./spongebob.jpg")

	output.WriteImage("./spongebob_deepfryed.jpg")
}
