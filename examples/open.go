package main

import (
	"gitlab.com/ruivieira/deepfryer"
	"image"
)

func main() {
	var config = deepfryer.DeepFryConfig{
		Levels:      8,
		ResizeBlur:  2.0,
		Brightness:  4,
		Contrast:    4,
		Compression: 10,
	}
	flareSize := uint(75)
	flareX := 155
	flareY := 125
	var flares = make([]deepfryer.Flare, 2)
	flares[0] = deepfryer.Flare{
		Position: image.Point{flareX, flareY},
		Size:     flareSize,
	}
	flares[1] = deepfryer.Flare{
		Position: image.Point{flareX + 40, flareY + 5},
		Size:     flareSize,
	}
	output := deepfryer.Deepfry(
		config,
		flares,
		"./open.jpg")

	output.WriteImage("./open_deepfryed.jpg")
}
