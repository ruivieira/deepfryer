package main

import (
	"gitlab.com/ruivieira/deepfryer"
	"image"
)

func main() {
	var config = deepfryer.DeepFryConfig{
		Levels:      8,
		ResizeBlur:  2.0,
		Brightness:  4,
		Contrast:    4,
		Compression: 10,
		Swirl:       150,
	}
	flareSize := uint(100)
	flareX := -10
	flareY := -10
	var flares = make([]deepfryer.Flare, 2)
	flares[0] = deepfryer.Flare{
		Position: image.Point{flareX, flareY + 25},
		Size:     flareSize,
	}
	flares[1] = deepfryer.Flare{
		Position: image.Point{flareX + 65, flareY},
		Size:     flareSize + 10,
	}
	output := deepfryer.Deepfry(
		config,
		flares,
		"./homer.jpg")

	output.WriteImage("./homer_deepfryed.jpg")
}
